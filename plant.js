const plantData = require('./plantData.json')

let context = null
let sink = null

function audioChain (...nodes) {
  nodes.reduce((current, next) => {
    current.connect(next)
    return next
  })
}

class PlantSynth {
  constructor(freq, amp, pan, timbre = 'sine') {
    this.params = {
      freq, amp, pan, timbre
    }
    this.start()
  }

  start() {
    const {freq, amp, pan, timbre} = this.params

    const node = context.createOscillator()
    node.type = timbre
    node.frequency.value = freq
    const gain = context.createGain()
    gain.gain.value = 0
    gain.gain.linearRampToValueAtTime(amp, context.currentTime + 1)
    gain.gain.linearRampToValueAtTime(0.0, context.currentTime + 30)

    const panner = context.createStereoPanner()
    panner.pan.value = pan

    audioChain(node, gain, panner, sink)
    node.start(0)

    this.playing = true
    this.node = node
    return node
  }

  stop(time = 0) {
    this.playing = false
    this.node.stop(time)
  }
}

function stopAll (nodes) {
  nodes.forEach(node => node.stop(context.currentTime + 60))
}

document.getElementById('play').addEventListener("click", () => {
  context = new AudioContext()
  const compressor = context.createDynamicsCompressor()
  compressor.threshold.setValueAtTime(-50, context.currentTime);
  compressor.knee.setValueAtTime(40, context.currentTime);
  compressor.ratio.setValueAtTime(12, context.currentTime);
  compressor.attack.setValueAtTime(0, context.currentTime);
  compressor.release.setValueAtTime(0.25, context.currentTime);
  compressor.connect(context.destination)
  sink = compressor

  const plants = new Map(
    plantData
      .filter(({Quadrat}) => Quadrat === "97.00")
      .map(({Latin, Frequency, Amplitude, Panning, Wave}) =>
        [Latin, new PlantSynth(Frequency, Amplitude, Panning, Wave)]
      ))
  console.log(`started ${plants.size} plants`)
})

// stopAll([plant1, plant2, plant3, plant4, plant5, plant6, plant7, plant8])
