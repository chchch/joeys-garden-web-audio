record PlantCSV {
  latin: String,
  common: String,
  frequency: Number,
  amplitude: Number,
  pan: Number,
  wave: String
  quadrat: Number,
  ordinal: Number
}

record Plant {
  latinName: String,
  commonName: String,
  frequency: Number,
  amplitude: Number,
  panning: Number,
  waveType: String
}

fun plantCSVtoPlant(p: PlantCSV) : Plant {
    {
    latinName: p.latin,
    commonName: p.common,
    frequency: p.frequency,
    amplitude: p.amplitude,
    panning: p.pan,
    waveType: p.wave,
    }
}

record Quadrat {
  name: Number,
  plants: Array(Plant)
}

/*
fun plantCSVsToQuadrat(ps: Array(PlantCSV)) : Array(Quadrat) {
    # group ps according to quadrat
    let m = ps.groupBy(.quadrat)  # giving a Map(Number,Collection(PlantCSV))
    # turn to Quadrat records:
    m.map(fun (quadrat: Number, ps: Collection(PlantCSV)) {
             Quadrat(quadrat, ps.map(plantCSVtoPlant)) })
}
*/


store Plants {
  state quadrats : Map(String, Quadrat) = Map.empty()
    |> Map.set("Quadrat 1", {
      name = "Quadrat 1",
      plants = [
        {
          latinName = "Peperomia obtusifolia",
          commonName = "Florida peperomia, Baby rubberplant",
          frequency = 0,
          amplitude = 0,
          panning = 0,
          waveType = "sine"
        },
        {
          latinName = "Yucca filamentosa",
          commonName = "Adam's needle",
          frequency = 0,
          amplitude = 0,
          panning = 0,
          waveType = "sine"
        }
      ]
    })
    |> Map.set("Quadrat 12", {
      name = "Quadrat 12",
      plants = []
    })
  state selected : String = "Quadrat 1"
  state active : Set(String) = Set.empty()

  get names : Array(String) {
    Map.values(quadrats)
    |> Array.map((q : Quadrat) : String { q.name })
  }

  get plants : Array(Plant) {
    case (Map.get(selected, quadrats)) {
      Maybe::Just quadrat => quadrat.plants
      Maybe::Nothing => []
    }
  }

  fun select(name : String) : Promise(Never, Void) {
    sequence {
      next {
        selected = name
      }
      next {
        active = Set.empty()
      }
    }
  }

  fun toggle(name : String) : Promise(Never, Void) {
    next {
      active = active |> if (Set.has(name, active)) {
        Set.delete(name)
      } else {
        Set.add(name)
      }
    }
  }

  fun plantActive(name : String) : Bool {
    Set.has(name, active)
  }
}
