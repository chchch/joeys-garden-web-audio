component QuadratList {
  connect Plants exposing { select, selected, names }

  style quadrats {
    margin-top: 8px;
    margin-left: 8px;
    border: 1px solid rgb(62, 68, 70);
    background: rgb(32, 35, 36);
    padding: 0px;
    width: 451px;
    color: rgb(232, 230, 227);
    font-size: 12px;
    font-weight: 400;
  }

  style quadrat(name : String) {
    list-style: none;
    padding: 11px 16px;
    background: #{if (name == selected) { "rgb(53, 57, 59)" } else { "inherit" }};
  }

  fun render : Html {
    <ul::quadrats>
      <{ list }>
    </ul>
  } where {
    list = names
      |> Array.map((name : String) : Html {
          <li::quadrat(name) onClick={() { select(name) }}><{ name }></li>
      })
  }
}
