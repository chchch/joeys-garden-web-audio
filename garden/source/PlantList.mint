component PlantList {
  connect Plants exposing { plants, toggle, plantActive }

  style plants {
    list-style: none;
    padding: 8px 12px;
    margin: 0px;
    a {
      text-decoration: none;
    }
  }

  style plant(name : String) {
    color: rgb(207, 203, 197);
    font-style: #{if (plantActive(name)) { "default" } else { "italic" }};
    font-weight: 400;
    line-height: 30px;
    font-size: 16px;
    font-family: serif;
  }

  fun render : Html {
    <ul::plants>
      <{ list }>
    </ul>
  } where {
    list = plants
      |> Array.map((plant : Plant) : Html {
        <a href="#" role="button" aria-pressed="false" onClick={() { toggle(plant.latinName) }}>
        <li::plant(plant.latinName)>
          <{
            if (plantActive(plant.latinName)) { plant.commonName} else { plant.latinName }
          }>
        </li>
        </a>
      })
  }
}
