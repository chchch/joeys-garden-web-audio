
To use csv-to-mint, run:

    git submodule init 
    git submodule update

To install the relevant dependencies in Debian run:

    apt install libtext-csv-perl libjson-perl   # Text::CSV JSON

Alternatively, to enter a nix environment in which the perl dependencies are available run:

    nix-shell

To use csv-to-mint, run:

    ./scripts/csv-to-mint --help

